const webpackConfig = require('./webpack.config.js');
const timer = require("grunt-timer");

module.exports = function(grunt) {
    // Initialize timer
    timer.init(grunt, {
        deferLogs: true
    });

    // Load tasks
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-postcss");
    grunt.loadNpmTasks("grunt-webpack");

    // Configure tasks
    grunt.initConfig({
        sass: {
            build: {
                options: { style: "expanded" },
                files: {
                    "dist/css/bootstrap-plus.css": "src/scss/build.scss"
                }
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require("pixrem")(),
                    require("autoprefixer")({
                        overrideBrowserslist: ["last 4 version", "> 0.1%", "ie >= 8", "ff >= 3"]
                    })
                ]
            },
            build: {
                src: "dist/css/bootstrap-plus.css"
            }
        },
        cssmin: {
            build: {
                options: {
                    sourceMap: true,
                    processImport: false // may not work
                },
                files: {
                    "dist/css/bootstrap-plus.min.css": "dist/css/bootstrap-plus.css"
                }
            },
        },
        webpack: {
            dev: webpackConfig({production: false}),
            prod: webpackConfig({production: true})
        },
        copy: {
            bootstrap_js: {
				expand: true,
				cwd: "node_modules/bootstrap/dist/js",
				dest: "dist/bootstrap/js",
				src: [
					"bootstrap*"
				]
			},
			bootstrap_css: {
				expand: true,
				cwd: "node_modules/bootstrap/dist/css",
				dest: "dist/bootstrap/css",
				src: [
					"bootstrap*"
				]
			},
            bootstrap_icons: {
				expand: true,
				cwd: "node_modules/bootstrap-icons/font",
				dest: "dist/bootstrap/icons",
				src: [
					"bootstrap-icons.css",
					"fonts/*"
				]
			}
        },
        watch: {
			css: {
				files: [
					'src/scss/**.scss', 'src/scss/**/**.scss'
				],
				tasks: [
					"sass",
					"postcss"
				],
				options: {
					spawn: false,
				}
			},
            js: {
				files: [
					'src/js/**.js', 'src/js/**/**.js'
				],
				tasks: [
					"webpack:dev"
				],
				options: {
					spawn: false,
				}
			}
		}
    });

    // Register tasks
    grunt.registerTask("default", [
        "sass",
        "postcss",
        "cssmin",
        "webpack"
    ]);

    grunt.registerTask("build:all", [
        "sass",
        "postcss",
        "cssmin",
        "webpack",
        "copy"
    ]);

    grunt.registerTask("build:dev", [
        "sass",
        "postcss",
        "webpack:dev"
    ]);

    grunt.registerTask("build:prod", [
        "sass",
        "postcss",
        "cssmin",
        "webpack:prod"
    ]);
};