#!/usr/bin/env bash

if [ -d "./.tmp.git" ]
then
	mv "./.tmp.git" "./.git"
fi

git pull

if [ -d "./.git" ]
then
	mv "./.git" "./.tmp.git"
fi
