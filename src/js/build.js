var _ = {
    version: '1.0.0'
};

var bootstrap_plus = {
    version: function() {
        return _.version;
    }
};

module.exports = bootstrap_plus;