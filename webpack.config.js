const path = require('path');

module.exports = env => {
    return {
        entry: './src/js/build.js',
        output: {
            path: path.resolve(__dirname + '/dist/js'),
            filename: env.production ? 'bootstrap.min.js' : 'bootstrap.js',
            library: {
                name: 'bootstrap_plus',
                type: 'umd'
            }
        },
        mode: env.production ? 'production' : 'development',
        module: {
            rules: [
                {
                    test: /.png$/,
                    use: 'base64-image-loader'
                },
                {
                    test: /.css$/,
                    use: 'css-content-loader'
                }
            ].concat(env.production ? [
                {
                    test: /\.(?:js|mjs|cjs)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['@babel/preset-env', { targets: "> 0.25%, not dead" }]
                            ]
                        }
                    }
                }
            ] : [])
        }
    }
};